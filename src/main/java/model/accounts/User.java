package model.accounts;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {

    private Long userId;
    private String username;
    private String password;
    private String passwordConfirm;
    private int gold;
    private Set<UserGames> userGames;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {

        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {

        this.passwordConfirm = passwordConfirm;
    }

    public int getGold() {

        return gold;
    }

    public void setGold(int gold) {

        this.gold = gold;
    }

    @OneToMany(mappedBy = "user")
    public Set<UserGames> getUserGames() {

        return userGames;
    }

    public void setUserGames(Set<UserGames> userGames) {

        this.userGames = userGames;
    }

}
