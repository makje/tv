package model.accounts;

import javax.persistence.*;;
import java.util.Date;

@Entity
@Table(name = "user_games")
public class UserGames {

    private Long gameId;
    private User user;
    private String userName;
    private String gameName;
    private Date dateOfPurchase;
    private Integer cost;
    private boolean returnable;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getGameId() {

        return gameId;
    }

    public void setGameId(Long gameId) {

        this.gameId = gameId;
    }

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public String getUserName() {

        return userName;
    }

    public void setUserName(String userName) {

        this.userName = userName;
    }

    public String getGameName() {

        return gameName;
    }

    public void setGameName(String gameName) {

        this.gameName = gameName;
    }

    public Date getDateOfPurchase() {

        return dateOfPurchase;
    }

    public void setDateOfPurchase(Date dateOfPurchase) {

        this.dateOfPurchase = dateOfPurchase;
    }

    public Integer getCost() {

        return cost;
    }

    public void setCost(Integer cost) {

        this.cost = cost;
    }

    @Transient
    public boolean isReturnable() {

        return returnable;
    }

    public void setReturnable(boolean returnable) {

        this.returnable = returnable;
    }

}
