package core.auth.service;

import model.accounts.User;

public interface UserService {
    void save(User user);
}