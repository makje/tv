package core.rest.service;

import core.shop.service.ShopService;
import model.games.Games;
import model.streams.Datum;
import model.streams.Streams;
import model.view.ChannelInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class TwitchServiceImpl implements TwitchService {

    @Autowired
    private ShopService shopService;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${twitch.key}")
    private String key;
    @Value("${t.value}")
    private String value;
    @Value("${game.name.api.url}")
    private String gameName;
    @Value("${top.streams.20.api.url}")
    private String streams20;
    @Value("${width.img}")
    private String width;
    @Value("${height.img}")
    private String height;
    @Value("${percent}")
    private int percent;

    public List<ChannelInfo> viewStreams() {
        Streams streams = getStreams();
        Games games = getGamesNames(streams);

        parseThumbnailUrlSize(streams);
        parseBoxArtUrl(games);

        List<ChannelInfo> channelInfoList = setChannelInfo(streams, games);
        return channelInfoList;
    }

    private List<ChannelInfo> setChannelInfo(Streams streams, Games games) {
        List<ChannelInfo> channelInfoList = new LinkedList<>();

        Map<String, model.games.Datum> map = new HashMap<>();
        for (model.games.Datum datum : games.getData()) {
            map.put(datum.getId(), datum);
        }

        for (Datum datum : streams.getData()) {
            ChannelInfo channelInfo = new ChannelInfo();

            channelInfo.setUserName(datum.getUserName());
            channelInfo.setTitle(datum.getTitle());
            channelInfo.setViewerCount(datum.getViewerCount());
            channelInfo.setStartedAt(datum.getStartedAt());
            channelInfo.setLanguage(datum.getLanguage());
            channelInfo.setThumbnailUrl(datum.getThumbnailUrl());

            String tmp1GameName = map.get(datum.getGameId()).getName();
            channelInfo.setGameName(tmp1GameName);

            String tmpBoxArtUrl = map.get(datum.getGameId()).getBoxArtUrl();
            channelInfo.setBoxArtUrl(tmpBoxArtUrl);

            channelInfo.setPrice(calculatePrice(datum.getViewerCount()));

            channelInfo.setBought(shopService.checkIfBought(datum.getUserName()));

            channelInfoList.add(channelInfo);
        }

        return channelInfoList;
    }

    private Integer calculatePrice(Integer popularity) {

        return popularity.intValue() / percent;
    }

    private Streams getStreams() {
        ResponseEntity<Streams> response = restTemplate.exchange(streams20, HttpMethod.GET, getHeaders(), Streams.class);

        return response.getBody();
    }

    private Games getGamesNames(Streams streams) {
        String gameId = "";
        for (Datum datum : streams.getData()) {
            gameId = gameId + datum.getGameId() + "&id=";
        }
        ResponseEntity<Games> response = restTemplate.exchange(gameName + gameId, HttpMethod.GET, getHeaders(), Games.class);

        return response.getBody();
    }

    private HttpEntity<String> getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(key, value);
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return entity;
    }

    private void parseThumbnailUrlSize(Streams streams) {
        for (Datum datum : streams.getData()) {
            datum.setThumbnailUrl(parseSize(datum.getThumbnailUrl()));
        }
    }

    private void parseBoxArtUrl(Games games) {
        for (model.games.Datum datum : games.getData()) {
            datum.setBoxArtUrl(parseSize(datum.getBoxArtUrl()));
        }
    }

    private String parseSize(String tmp) {
        tmp = tmp.replace("{width}", width).replace("{height}", height);

        return tmp;
    }

}
