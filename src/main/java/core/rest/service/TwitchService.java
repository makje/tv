package core.rest.service;

import model.view.ChannelInfo;

import java.util.List;

public interface TwitchService {
    List<ChannelInfo> viewStreams();
}