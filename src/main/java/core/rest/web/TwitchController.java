package core.rest.web;

import core.rest.service.TwitchService;
import core.shop.service.ShopService;
import model.view.ChannelInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;

@Controller
public class TwitchController {

    @Autowired
    private TwitchService twitchService;

    @Autowired
    private ShopService shopService;

    @RequestMapping(value = "streams", method = RequestMethod.GET)
    public String streams(Model model) {
        try {
            List<ChannelInfo> channelInfoList = twitchService.viewStreams();
            model.addAttribute("streams", channelInfoList);

            return "streams";
        } catch (ResourceAccessException e) {
            model.addAttribute("message", "Twitch unreachable connection.");

            return "error";
        }

    }

    @RequestMapping(value = "/twitch", method = RequestMethod.GET)
    public String twitch(@RequestParam(value = "userName", defaultValue = "monstercat") String userName, Model model) {
        model.addAttribute("channel", userName);

        if (shopService.checkIfBought(userName))
            return "twitch";

        return "twitch-demo";
    }//doubled for the job.

}
