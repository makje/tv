package core.shop.repository;

import model.accounts.UserGames;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopRepository extends JpaRepository<UserGames, Long> {
}
