package core.shop.service;

import model.accounts.UserGames;

import java.util.List;

public interface ShopService {
    boolean buy(String userName, String gameName, Integer cost);
    void refund(String userName);
    List<UserGames> viewBoughtGames();
    boolean checkIfBought(String userName);
    int getCash();
}