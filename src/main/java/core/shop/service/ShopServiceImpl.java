package core.shop.service;

import core.auth.repository.UserRepository;
import core.auth.service.SecurityService;
import core.shop.repository.ShopRepository;
import model.accounts.User;
import model.accounts.UserGames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ShopServiceImpl implements ShopService {

    @Value("${number.of.days.to.return}")
    private float expiredTime;

    @Value("${ms.in.day}")
    private long msInDay;

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityService securityService;

    @Transactional
    public boolean buy(String userName, String gameName, Integer cost) {
        Set<UserGames> userGamesSet = new HashSet<UserGames>();
        UserGames userGames = new UserGames();
        User user = userRepository.findByUsername(getLoggedInUsername());

        int result = user.getGold() - cost.intValue();
        if (result >= 0) {
            user.setGold(result);

            userGames.setUser(user);
            userGames.setUserName(userName);
            userGames.setGameName(gameName);
            userGames.setCost(cost);
            userGames.setDateOfPurchase(new Date());

            userGamesSet.add(userGames);

            userRepository.save(user);
            shopRepository.save(userGames);

            return true;
        } else
            return false;

    }

    @Transactional
    public void refund(String userName) {
        UserGames probe = new UserGames();
        User user = userRepository.findByUsername(getLoggedInUsername());

        probe.setUser(user);
        probe.setUserName(userName);

        List<UserGames> userGamesList = shopRepository.findAll(Example.of(probe));
        for (UserGames userGames : userGamesList) {
            int result = user.getGold() + userGames.getCost();
            user.setGold(result);

            userRepository.save(user);
            shopRepository.delete(userGames);
        }

    }

    public List<UserGames> viewBoughtGames() {
        UserGames probe = new UserGames();
        probe.setUser(userRepository.findByUsername(getLoggedInUsername()));

        List<UserGames> userGamesList = shopRepository.findAll(Example.of(probe));
        for (UserGames userGames : userGamesList) {
            userGames.setReturnable(checkIfReturnable(userGames));
        }

        return userGamesList;
    }

    public boolean checkIfBought(String userName) {
        UserGames probe = new UserGames();
        probe.setUser(userRepository.findByUsername(getLoggedInUsername()));
        probe.setUserName(userName);

        return shopRepository.exists(Example.of(probe));
    }

    public int getCash() {
        User user = userRepository.findByUsername(getLoggedInUsername());

        return user.getGold();
    }

    private boolean checkIfReturnable(UserGames userGames) {
        Date date1 = userGames.getDateOfPurchase();
        Date date2 = new Date();

        long diff = date2.getTime() - date1.getTime();
        float days = (diff / (msInDay));

        if (days < expiredTime)
            return true;
        else
            return false;
    }

    private String getLoggedInUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        return auth.getName();
    }

}
