package core.shop.web;

import core.shop.service.ShopService;
import model.accounts.UserGames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ShopControler {

    @Autowired
    ShopService shopService;

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public String account(Model model) {

        model.addAttribute("gold", shopService.getCash());

        List<UserGames> userGamesList = shopService.viewBoughtGames();
        model.addAttribute("bought", userGamesList);

        return "manager";
    }

    @RequestMapping(value = "/manager", method = RequestMethod.GET)
    public String twitch(@RequestParam(value = "id") String id,
                         @RequestParam(value = "gameName") String gameName,
                         @RequestParam(value = "price") Integer price, Model model) {

        if (shopService.checkIfBought(id))
            model.addAttribute("message", "The game has already been bought.");

        else if (!shopService.buy(id, gameName, price))
            model.addAttribute("message", "Not enough gold.");

        model.addAttribute("gold", shopService.getCash());

        List<UserGames> userGamesList = shopService.viewBoughtGames();
        model.addAttribute("bought", userGamesList);

        return "manager";
    }

    @RequestMapping(value = "/refund", method = RequestMethod.GET)
    public String erase(@RequestParam(value = "id") String id, Model model) {

        shopService.refund(id);

        model.addAttribute("gold", shopService.getCash());

        List<UserGames> userGamesList = shopService.viewBoughtGames();
        model.addAttribute("bought", userGamesList);

        return "manager";
    }

}
